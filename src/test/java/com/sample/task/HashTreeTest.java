package com.sample.task;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HashTreeTest {
    private final String SAMPLE_VALUE = "Fourth";
    private final List<String> INPUTS = Arrays.asList(
            "First", "Second", "Third", SAMPLE_VALUE, "Fifth", "Sixth", "Seventh", "Eighth"
    );

    @Test
    public void shouldFindPathToRoot() {
        HashTree hashTree = new HashTreeCreator().buildTree(INPUTS);
        Optional<List<String>> hashesPaths = hashTree.findPathByValue(HashUtil.hashString(SAMPLE_VALUE));

        assertTrue(hashesPaths.isPresent());
        assertEquals(3, hashesPaths.get().size());
    }

    @Test
    public void shouldFindPathToRootWithUnevenLeaves() {
        List<String> lines = Arrays.asList(
                "First", "Second", SAMPLE_VALUE, "Fourth", "Fifth"
        );

        HashTree hashTree = new HashTreeCreator().buildTree(lines);
        Optional<List<String>> hashesPaths = hashTree.findPathByValue(HashUtil.hashString(SAMPLE_VALUE));

        assertTrue(hashesPaths.isPresent());
        assertEquals(3, hashesPaths.get().size());
    }
}