package com.sample.task;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class HashTreeCreatorTest {
    private final String SAMPLE_VALUE = "Fourth";
    private final HashTreeCreator hashTreeCreator = new HashTreeCreator();

    @Test
    public void shouldBuildHashTreeWithUnevenNumberOfLines(){
        List<String> lines = Arrays.asList(
                "First", "Second", "Third", SAMPLE_VALUE, "Fifth"
        );
        HashTree hashTree = hashTreeCreator.buildTree(lines);

        assertTrue(hashTree.getRoot().getHash() != null);
        assertNull(hashTree.getRoot().getTop());
    }

    @Test
    public void shouldBuildHashTreeWithEvenNumberOfLines() {
        List<String> lines = Arrays.asList(
                "First", "Second", "Third", SAMPLE_VALUE, "Fifth", "Sixth", "Seventh", "Eighth"
        );
        HashTree hashTree = hashTreeCreator.buildTree(lines);

        assertTrue(hashTree.getRoot().getHash() != null);
        assertNull(hashTree.getRoot().getTop());
    }

    @Test
    public void shouldSetSignatureForRoot(){
        List<String> lines = Arrays.asList("1", "2");
        HashTree hashTree = hashTreeCreator.buildTree(lines);

        assertNotNull(hashTree.getRoot().getSignature());
    }
}