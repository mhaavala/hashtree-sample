package com.sample.task;

import org.junit.Test;

import static org.junit.Assert.*;

public class HashUtilTest {
    private final String INPUT = "FIRST LINE";

    @Test
    public void shouldHashInput() {
        String expectedHash = "b185a706609e91238e3f1fe2433f0394f917f13e3f9daa9ec9e840f674f98daa";

        String value = HashUtil.hashString(INPUT);

        assertEquals(expectedHash, value);
    }
}