package com.sample.task;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private Node left;
    private Node right;
    private Node top;
    private String hash;
    private byte[] signature;

    private boolean isLeafNode;

    public Node(Node left, Node right) {
        this.left = left;
        this.right = right;

        this.attachTop(left);
        this.attachTop(right);
    }

    public Node(String hash) {
        this.hash = hash;
    }

    public static Node newLeafNode(String hash) {
        Node node = new Node(hash);
        node.isLeafNode = true;

        return node;
    }

    public Node withValue(String hash) {
        this.hash = hash;

        return this;
    }

    public List<Node> getPathToRoot() {
        List<Node> nodes = new ArrayList<>();
        Node node = this;

        while(node.getTop() != null) {
            nodes.add(node);
            node = node.getTop();
        }

        return nodes;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public boolean isLeafNode() {
        return isLeafNode;
    }

    public void setLeafNode(boolean isLeafNode) {
        isLeafNode = isLeafNode;
    }

    private void attachTop(Node childNode) {
        childNode.setTop(this);
    }

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        String characteristics = isLeafNode ? "isLeaf: true" : String.format("left: %s, right: %s", left, right);

        return String.format("com.sample.task.Node[hash:%s, %s] \n", hash, characteristics);
    }
}
