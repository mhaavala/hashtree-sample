package com.sample.task;

import com.google.common.base.Charsets;

public class Signer {
    public byte[] sign(String value) {
        // TODO - Signature integration needed
        return value.getBytes(Charsets.UTF_8);
    }
}
