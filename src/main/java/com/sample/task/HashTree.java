package com.sample.task;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class HashTree {
    private final Node root;

    public HashTree(Node root) {
        this.root = root;
        this.sign();
    }

    public Node getRoot() {
        return this.root;
    }

    public Optional<List<String>> findPathByValue(String value) {
        Node node = traverse(this.root, value);

        if(node == null) {
            return Optional.empty();
        }

        return Optional.of(
                node.getPathToRoot()
                        .stream()
                        .map(n -> n.getHash())
                        .collect(Collectors.toList())
        );
    }

    private void sign() {
        this.root.setSignature(new Signer().sign(this.root.getHash()));
    }

    private Node traverse(Node node, String value) {
        if (node == null) {
            return null;
        }
        if (node.getHash().equals(value)) {
            return node;
        }
        Node left = traverse(node.getLeft(), value);
        if (left != null) {
            return left;
        }
        Node right = traverse(node.getRight(), value);
        if (right != null) {
            return right;
        }

        return null;
    }
}
