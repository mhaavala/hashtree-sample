package com.sample.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HashTreeCreator {

    public HashTree buildTree(List<String> lines) {
        List<Node> leafNodes = lines.stream()
                .map(line -> hash(line))
                .map(hash -> Node.newLeafNode(hash))
                .collect(Collectors.toList());

        return new HashTree(construct(leafNodes).get(0));
    }

    private List<Node> construct(List<Node> nodes) {
        if (nodes.size() <= 1) {
            return nodes;
        }
        List<Node> results = new ArrayList<>();
        for (int i = 0; i < nodes.size() - 1; i += 2) {
            results.add(constructParentNode(nodes.get(i), nodes.get(i + 1)));
        }

        if (isEven(nodes.size()) ) {
            results.add(constructNodeWithDuplication(nodes.get(nodes.size() - 1)));
        }

        return construct(results);
    }

    private String hash(String input) {
        return HashUtil.hashString(input);
    }

    private boolean isEven(int size) {
        return size % 2 != 0;
    }

    private Node constructParentNode(Node leftLeaf, Node rightLeaf) {
        return new Node(leftLeaf, rightLeaf).withValue(combineHashes(leftLeaf, rightLeaf));
    }

    private Node constructNodeWithDuplication(Node node) {
        Node dummyLeaf = new Node(node.getHash());

        return new Node(node, dummyLeaf).withValue(combineHashes(node, dummyLeaf));
    }

    private String combineHashes(Node leftLeaf, Node rightLeaf) {
        return hash(leftLeaf.getHash() + rightLeaf.getHash());
    }
}
