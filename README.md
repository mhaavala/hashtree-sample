## Sample HashTree implementation

### Build

        $ ./gradlew

### Usage

Build the HashTree:

```java
            List<String> logLines = Arrays.asList(
                    "First", "Second", "Third", "Fourth", "Fifth"
            );

            HashTree hashTree = new HashTreeCreator().buildTree(lines);
```

Find the path from the value to the root:


```java
           Optional<List<String>> hashesPaths = hashTree.findPathByValue(HashUtil.hashString("Sample"));
```